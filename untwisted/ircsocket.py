import socket
import logging
import errno
from istring import istring

class IRCSocket():

	events = 0
	_log = logging.getLogger(__name__)
	_sock = None
	_reactor = None
	_spillover = ""
	_out_buffer = ""
	_pendingConnect = False

	def __init__(self, reactor):
		self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self._sock.setblocking(0)
		self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
		self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

		self._reactor = reactor
		self._reactor.engine.RegisterSocket(self)
	
	def Shutdown(self):
		self._reactor.engine.UnregisterSocket(self)
		self._sock.close()

	def ProcessRead(self):
		try:
			data = self._sock.recv(4096)
		except Exception, ex:
			self._log.error("Error reading from socket: %s" % ex)
			raise Exception("Error reading from socket: %s" % ex)
		if len(data) == 0:
			self._log.info("Lost connection")
			raise Exception("Lost connection")

		self._spillover += data

		lines = self._spillover.split("\n")
		if self._spillover[-1] != '\n':
			self._spillover = lines.pop()
		else:
			self._spillover = ""
		for line in lines:
			line = line.strip("\r\n")
			if line:
				self.OnRead(istring(line))
	
	def Write(self, data):
		if isinstance(data, unicode):
			data = data.encode('utf-8')
		self._out_buffer += data + "\n"
		self._reactor.engine.RequestWrite(self)

	def ProcessWrite(self):
		if self._pendingConnect:
			self._pendingConnect = False
			try:
				i = self._sock.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR)
				if i != 0:
					self.OnConnectError(i)
					return
				else:
					self.OnConnectSuccess()
					self._reactor.engine.RequestRead(self)
					return
			except Exception, ex:
				self.OnConnectError(ex)
				return

		if len(self._out_buffer) == 0:
			self._reactor.engine.CancelWrite(self)
			return

		sent = self._sock.send(self._out_buffer)
		if sent == -1:
			self._log.error("Error writing to socket: %s" % ex)
			raise Exception("Error writing to socket: %s" % ex)

		self._out_buffer = self._out_buffer[sent:]
		if len(self._out_buffer) == 0:
			self._reactor.engine.CancelWrite(self)
	
	def ProcessError(self):
		self.Shutdown()
		if self._pendingConnect:
			self._pendingConnect = False
			i = 0
			try:
				i = socket.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR)
			except Exception, ex:
				i = ex.errno
			self.OnConnectError(i)
		self._log.error("Error on socket")
		self.OnError("Lost connection")
	
	def Bind(self, address):
		self._sock.bind((address, 0))
	
	def Connect(self, address, port):
		try:
			self._sock.connect((address, port))
			self.OnConnectStarted(address, port)
			self.OnConnectSuccess()
			self._reactor.engine.RequestRead(self)
		except Exception, ex:
			if ex.errno == errno.EINPROGRESS:
				self._pendingConnect = True
				self._reactor.engine.RequestWrite(self)
				self.OnConnectStarted(address, port)
			else:
				raise
	
	def GetFD(self):
		return self._sock.fileno()
	
	def OnConnectError(self, err):
		pass
	
	def OnConnectStarted(self, addr, port):
		pass
	
	def OnConnectSuccess(self):
		pass
	
	def OnRead(self):
		pass
	
	def OnError(self, ex):
		pass

