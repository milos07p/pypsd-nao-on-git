#### NAMING ####
Module filenames must be in the format of "psm_yourmodulenamehere.py" preferrably all lowercase
Module classes must be in the format of "PSModule_yourmodulenamehere", with the modulename being the same case as what you used in the filename

#### IMPORTS ####
All modules must import:
---
from psmodule import *

#### CLASS ####
All modules must extend:
---
PSModule

#### CLASS VARIABLES ####
You should implement class variables:
---
VERSION = (int), up to you
---

Additionally, the default __init__ sets several useful class variables for you to use in your module:
---
parent = object (received from __init__, your parent server object, e.g., TS6Protocol())
config = object (received from __init__, the same configuration object used in the parent server object)
logchan = string (is set in __init__, channel from config where your bot users reside if you use one)
log = object (is set in __init__, file/stdout logger (usage: log.error(msg)|log.info(msg)...etc, see python logging lib for details)
dbp = object (is set in __init__, is the parent's database pointer) -- NB you should always make your own database if your module needs one

#### CLASS METHODS ####
You must implement:
---
def startup(self) [called when parent has connected and sync'd to network]
def shutdown(self) [called when module is unloaded; kill your user, stop activity, etc]
def getHooks(self) [called to receive hook data, see below]
---

You must begin your startup method like so:
---
def startup(self):
	if not PSModule.startup(self): return False
---

And similarly, begin your shutdown method like so:
---
def shutdown(self):
	PSModule.shutdown(self)
---

You may want to override / extend:
---
def __init__(self, parent, config) [called on module instantiation]
def getVersion(self) [called to retreive the VERSION int]

#### EVENT HOOKS ####
Modules can hook into the parent code at any point.  Currently hooks exist for 'uid' (when a user connects) and 'privmsg' (on receiving a message).
You can add extra hook points by modifying pseudoserver.py (see irc_UID() for proper usage).
Your hooks must be in the following format:
---
def yourmodulenamehere_hooknamehere(self, prefix, params)
---
Where hooknamehere is whatever you want to call your hook.  prefix and params are what get passed from the source hook method.
It is of utmost importance to prefix your hooks with your module's name, otherwise the unloading code will break.
After writing your hook method, you add it in a tuple of tuples to your getHooks() definition as in the example below:
---
def getHooks(self):
	return (('uid', self.bopm_SCANUSER),)
---

#### COMMAND HOOKS ####
You can register command handlers (issued over privmsg / chanmsg).  Here's how:
---
def getCommands(self):
	return (('sayhi',{
		'channel': ('#mychannel', '#myotherchannel'),
		'modes': 'r',
		'master': None,
		'callback': self.cmd_sayhello
		'usage': "<channel/user> - say hello to a user"
	}),...)
---
Obviously you don't have to define all the acl methods, but you could if you want.
ACL:
- Master means they're in the masters list
- Modes means they have usermode <<whatever you supply as the arg>>
- Channel means the command was used in one of those channels - this must always be a tuple or list
REQUIRED:
- callback - the method to receive the callback params = (source, target, pieces)
- usage - for help, use the syntax "<required> [optional] - what does this method do"
