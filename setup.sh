#!/bin/bash

PY_BASE="/home/adam/rizon/python"
PY_BIN="$PY_BASE/bin/python"
EASY_INSTALL="$PY_BASE/bin/easy_install"

if [ ! -f $EASY_INSTALL ] ; then
	cd $PY_BASE
	wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
	$PY_BIN ez_setup.py
fi

$EASY_INSTALL pymysql
$EASY_INSTALL networkx
$EASY_INSTALL BeautifulSoup
$EASY_INSTALL pyparsing
$EASY_INSTALL wordnik

pushd $PY_BASE
wget http://py-dom-xpath.googlecode.com/files/py-dom-xpath-0.1.tar.gz
tar zxf py-dom-xpath-0.1.tar.gz
cd py-dom-xpath-0.1
$PY_BIN setup.py install
popd
