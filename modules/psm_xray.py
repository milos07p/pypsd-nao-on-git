#!/usr/bin/python pseudoserver.py
#psm-xray.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
import re
import unicodedata as ud
from untwisted.istring import istring

class PSModule_xray(PSModule):
	VERSION = 5
	uid = ""
	xraychan = ""

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)
		
		try:
			self.xraychan = istring(config.get('xray', 'channel'))
		except Exception, err:
			self.log.exception("Error, setting 'xray:channel' has not been set in config: %s" % err)
			raise

	def startup(self):
		if not PSModule.startup(self): return False
		try:
			self.reload()
		except Exception, err:
			self.log.exception("Error reading configuration file for xray module: %s" % err)
			raise

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('xray', 'nick'),
				self.config.get('xray', 'user'),
				self.config.get('xray', 'host'),
				self.config.get('xray', 'gecos'),
				self.config.get('xray', 'modes'),
				self.config.get('xray', 'nspass'),
				join_chans = [self.xraychan,]
			)
		except Exception, err:
			self.log.exception("Error, config invalid for xray module: %s" % err)
			raise
			
		return True

	def shutdown(self):
		PSModule.shutdown(self)
		self.parent.quitFakeUser(self.uid)
		
	def fixRegexForLaura(self, regex):
		try:
			kregex = r'\\p\{(.*?)\}'

			m = re.findall(kregex, regex)
			#print m
			for cattype in m:
				repl = [unichr(x) for x in range(65536) if ud.category(unichr(x)).startswith(cattype)]
				ksubregex = r'(\\p{%s})' % cattype
				regex = re.sub(ksubregex, '[%s]' % ''.join(repl), regex)
			return regex
		except Exception, err:
			return regex #dont bother fixing if something breaks
			
	def xray_regex(self, nick=".+", user=".+", ip=".+", gecos=".+", version=".*", rdns=".+", variable_host=False, exact=False):
		try:
			if exact:
				nick = "^" + nick + "$"
				user = "^" + user + "$"
				ip = "^" + ip + "$"
				gecos = "^" + gecos + "$"
				version = "^" + version + "$"
				rdns = "^" + rdns + "$"
			nregex = re.compile(self.fixRegexForLaura(nick))
			uregex = re.compile(self.fixRegexForLaura(user))
			iregex = re.compile(self.fixRegexForLaura(ip))
			gregex = re.compile(self.fixRegexForLaura(gecos))
			vregex = re.compile(self.fixRegexForLaura(version))
			rregex = re.compile(self.fixRegexForLaura(rdns))
		except Exception, err:
			self.log.exception("Error compiling regex: %s" % err)
			return False
		
		if variable_host:
			return [user for uid,user in self.parent.user_t.iteritems()
					if nregex.match(user['nick']) and 
						uregex.match(user['user']) and 
						(
							iregex.match(user['ip']) or 
							iregex.match(user['rdns'])
						) and
						gregex.match(user['gecos']) and
						vregex.match(user['version'])
				]
		else:
			return [user for uid,user in self.parent.user_t.iteritems()
					if nregex.match(user['nick']) and 
						uregex.match(user['user']) and 
						iregex.match(user['ip']) and 
						rregex.match(user['rdns']) and
						gregex.match(user['gecos']) and
						vregex.match(user['version'])
				]

## Begin Command hooks		
	def cmd_regexip(self, source, target, pieces):
		if not pieces: return False
		
		curcount = 0
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100

		users = self.xray_regex(ip=pieces[0])
		
		for user in users:
			curcount +=1
			self.parent.notice(source, "RIP: %s!%s@%s|%s|%s (%s): %s" %
				(user['nick'], user['user'], user['ip'], user['rdns'], user['host'],
				user['gecos'], user['version']), self.uid)
			if curcount >= limiter: break
		self.parent.notice(source, "END OF RIP (%d/%d matches)" % (curcount, len(users)), self.uid)
		return True

	def cmd_regexnick(self, source, target, pieces):
		if not pieces: return False
		
		curcount = 0
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100

		users = self.xray_regex(nick=pieces[0])
		
		for user in users:
			curcount +=1
			self.parent.notice(source, "RNICK: %s!%s@%s|%s|%s (%s): %s" %
				(user['nick'], user['user'], user['ip'], user['rdns'], user['host'],
				user['gecos'], user['version']), self.uid)
			if curcount >= limiter: break
		self.parent.notice(source, "END OF RNICK (%d/%d matches)" % (curcount, len(users)), self.uid)
		return True

	def cmd_regexmask(self, source, target, pieces):
		if not pieces: return False

		curcount = 0
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100

		try:
			m = re.match("^([^!]+)\!([^@]+)\@([^:]+)\:(.*)$", pieces[0]).groups()
			if len(m) != 4: return False
		except Exception, err:
			self.log.exception("Error in rmask: %s" % (err,))

		users = self.xray_regex(m[0],m[1],m[2],m[3],variable_host=True,exact=True)
		for user in users:
			curcount +=1
			self.parent.notice(source, "RMASK: %s!%s@%s|%s|%s (%s): %s" %
				(user['nick'], user['user'], user['ip'], user['rdns'], user['host'],
				user['gecos'], user['version']), self.uid)
			if curcount >= limiter: break
		self.parent.notice(source, "END OF RMASK (%d/%d matches)" % (curcount, len(users)), self.uid)
		return True

	def cmd_regexmaskbr(self, source, target, pieces):
		if not pieces: return False

		curcount = 0
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100

		try:
			for uid, user in self.parent.user_t.iteritems():
			#	self.log.error("%s: %s" % (user, pieces[0]))
				try:
					m = re.match(pieces[0], "%s!%s@%s:%s" % (user['nick'], user['user'], user['ip'], user['gecos'])).groups()
					curcount += 1
					self.parent.notice(source, "RMASKBR: %s!%s@%s:%s" % (user['nick'], user['user'], user['ip'], user['gecos']), self.uid)
				except Exception, err:
					pass

				if curcount >= limiter: break
			self.parent.notice(source, "END OF RMASK (%d/>=%d matches)" % (curcount, limiter), self.uid)
			return True
		except Exception, err:
			self.log.exception("Error in rmask: %s" % (err,))
			return False

	def cmd_regexversion(self, source, target, pieces):
		if not pieces: return False
		
		curcount = 0
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100

		users = self.xray_regex(version=pieces[0])
		
		for user in users:
			curcount +=1
			self.parent.notice(source, "RVER: %s!%s@%s|%s|%s (%s): %s" %
				(user['nick'], user['user'], user['ip'], user['rdns'], user['host'],
				user['gecos'], user['version']), self.uid)
			if curcount >= limiter: break
		self.parent.notice(source, "END OF RVER (%d/%d matches)" % (curcount, len(users)), self.uid)
		return True
	
	def cmd_regextopic(self, source, target, pieces):
		if not pieces: return False
		
		curcount, matches = (0,0)
		try:
			limiter = int(pieces[-1])
		except (ValueError, IndexError):
			limiter = 100
			
		try:
			tregex = re.compile(self.fixRegexForLaura(pieces[0]))
		except Exception, err:
			self.log.exception("Error compiling regex: %s" % err)
			return False

		for cname, chan in self.parent.channel_t.iteritems():
			curcount +=1
			if tregex.search(chan['topic']):
				matches +=1
				self.parent.notice(source, "RTOP: %s(%d): %s" % (cname, len(chan['members']), chan['topic']), self.uid)
			if matches >= limiter: break
		self.parent.notice(source, "END OF RTOP (%d matches)" % (matches), self.uid)
		return True
			
		
## End Command hooks

	def getCommands(self):
		return (('rnick', {
			'permission':'x',
			'callback':self.cmd_regexnick,
			'usage':"<regex nick pattern> [max results|default=100] - returns a list of nicks matching regex"}),
			('rip', {
			'permission':'x',
			'callback':self.cmd_regexip,
			'usage':"<regex IP address pattern> [max results|default=100] - returns a list of nicks matching IP regex"}),
			('rmask', {
			'permission':'x',
			'callback':self.cmd_regexmask,
			'usage':"<regex nick pattern>!<regex user pattern>@<regex ip/rdns pattern>:<regex gecos pattern> [max results|default=100] - returns a list of nicks matching supplied regex"}),
			('rmaskbr', {
			'permission':'x',
			'callback':self.cmd_regexmaskbr,
			'usage':"<regex nick pattern>!<regex user pattern>@<regex ip/rdns pattern>:<regex gecos pattern> [max results|default=100] - returns a list of nicks matching supplied regex, slow but capable of backreferences"}),
			('rver', {
			'permission':'x',
			'callback':self.cmd_regexversion,
			'usage':"<regex CTCP VERSION pattern> [max results|default=100] - returns a list of nicks matching CTCP VERSION regex"}),
			('rtop', {
			'permission':'x',
			'callback':self.cmd_regextopic,
			'usage':"<regex TOPIC pattern> [max results|default=100] - returns a list of channels matching TOPIC regex"}),
		)
