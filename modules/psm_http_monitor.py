# psm_http_monitor.py
# Written by Adam (Adam@rizon.net)

from psmodule import *
from untwisted import task
from untwisted.istring import istring
import threading
import httplib
from time import sleep

class HTTPThread(threading.Thread):
	exiting = False
	exit_mutex = threading.Lock()

	def run(self):
		self.exit_mutex.acquire()
		while not self.exiting:
			try:
				self.exit_mutex.release()
				PSModule_http_monitor.http_mutex.acquire()
				http_list = list(PSModule_http_monitor.http_monitor)
				PSModule_http_monitor.http_mutex.release()
				for site in http_list:
					slash = site.find("/")
					if slash == -1:
						host = site
						directory = "/"
					else:
						host = site[0:slash]
						directory = site[slash:]

					con = httplib.HTTPConnection(host, timeout=10)
					try:
						con.request("GET", directory)
						res = con.getresponse()
						if res.status != 200:
							PSModule_http_monitor.error_mutex.acquire()
							PSModule_http_monitor.error_list.append((site, res.status))
							PSModule_http_monitor.error_mutex.release()
					except:
						PSModule_http_monitor.error_mutex.acquire()
						PSModule_http_monitor.error_list.append((site, 0))
						PSModule_http_monitor.error_mutex.release()
			except:
				pass
			sleep(300)
			self.exit_mutex.acquire()
		self.exit_mutex.release()

class PSModule_http_monitor(PSModule):
	http_monitor = []
	http_mutex = threading.Lock()
	error_list = []
	error_mutex = threading.Lock()
	lc = None
	channels = None

	def check_error(self):
		self.error_mutex.acquire()
		for site,code in self.error_list:
			if code == 0:
				self.parent.privMsg(self.channels, "HTTP Monitor: Unable to connect to " + site)
			else:
				self.parent.privMsg(self.channels, "HTTP Monitor: " + site + " returned error code " + str(code))
		del self.error_list[:]
		self.error_mutex.release()

	def startup(self):
		if not PSModule.startup(self):
			return False
		self.log = logging.getLogger(__name__)

		self.channels = istring(self.config.get('http_monitor', 'channel'))
		if not self.channels:
			self.log.exception("http_monitor:channel is not defined!")
			return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS http_monitor (site VARCHAR(255));")
		except Exception, err:
			self.log.exception("Error creating table for http_monitor module (%s)" % err)
			raise
		
		self.http_mutex.acquire()
		try:
			self.dbp.execute("SELECT * FROM http_monitor")
			for row in self.dbp.fetchall():
				self.http_monitor.append(row[0])
		except Exception, err:
			self.log.exception("Error loading http_monitor entries from database: %s" % err)
			self.http_mutex.release()
			raise
		self.http_mutex.release()

		t = HTTPThread()
		t.daemon = True
		t.start()

		self.lc = task.LoopingCall(self.check_error)
		self.lc.start(150)
		
		return True
	
	def shutdown(self):
		try:
			HTTPThread.exit_mutex.acquire()
			HTTPThread.exiting = True
			HTTPThread.exit_mutex.release()
			if self.lc:
				self.lc.stop()
		except:
			pass
	
	def cmd_httpAdd(self, source, target, pieces):
		if len(pieces) == 0:
			return False

		site = pieces[0].replace("http://", "")
		
		self.http_mutex.acquire()
		for m in self.http_monitor:
			if m == site:
				self.parent.privMsg(target, site + " is already on the HTTP monitor list")
				self.http_mutex.release()
				return True
		
		try:
			self.dbp.execute("INSERT INTO http_monitor (site) VALUES(%s)", (site))
			self.http_monitor.append(site)
			self.parent.privMsg(target, site + " added to the HTTP monitor list")
		except Exception, err:
			self.log.exception("Error inserting entry into http_monitor database: %s" % (err))
		self.http_mutex.release()
		
		return True
	
	def cmd_httpDel(self, source, target, pieces):
		if len(pieces) == 0:
			return False

		site = pieces[0].replace("http://", "")
		
		self.http_mutex.acquire()
		found = False
		for m in self.http_monitor:
			if m == site:
				try:
					self.dbp.execute("DELETE FROM http_monitor WHERE `site` = %s", site)
					self.http_monitor.remove(site)
					self.parent.privMsg(target, site + " removed from the HTTP monitor list")
					found = True
				except Exception, err:
					self.log.exception("Error removing entry from http_monitor database: %s" % (err))
				break
		if not found:
			self.parent.privMsg(target, site + " is not on the HTTP monitor list")
		self.http_mutex.release()

		return True
	
	def cmd_httpList(self, source, target, pieces):
		self.http_mutex.acquire()
		if len(self.http_monitor) == 0:
			self.parent.privMsg(target, "HTTP monitor list is empty.")
		else:
			self.parent.privMsg(target, "Currently monitoring:")
			for d in self.http_monitor:
				self.parent.privMsg(target, d)
		self.http_mutex.release()
		return True
	
	def getCommands(self):
		return (
			('add', {
			'permission' : 'M',
			'callback' : self.cmd_httpAdd,
			'usage' : "<site> - Add a site to the http monitor list"}),
			('del', {
			'permission' : 'M',
			'callback' : self.cmd_httpDel,
			'usage' : "<site> - Remove a site from the http monitor list"}),
			('list', {
			'permission' : 'M',
			'callback' : self.cmd_httpList,
			'usage' : "- Shows all http monitor entries"}),
		)

