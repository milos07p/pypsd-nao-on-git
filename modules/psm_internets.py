#!/usr/bin/python pseudoserver.py
# psm_internets.py
# module for pypseudoserver
# written by ElChE <elche@rizon.net>, martin <martin@rizon.net>

import sys
import traceback
import types
from untwisted.istring import istring
from datetime import datetime
from decimal import Decimal, InvalidOperation
from psmodule import *
from untwisted import threading
from internets import cmd_admin, cmd_manager, cmd_private, cmd_user
from internets import sys_antiflood, sys_auth, sys_channels, sys_log, sys_options, sys_users, erepparser
from internets.api import bing, calc, google, imdb, ipinfo, lastfm, quotes, urbandictionary, urls, weather, wolfram, words
from internets.utils import *

class PSModule_internets(PSModule):
	VERSION = 2
	VERSION_MINOR = 0
	initialized = False

	def bind_function(self, function):
		func = types.MethodType(function, self, PSModule_internets)
		setattr(PSModule_internets, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'e', 'callback': self.bind_function(list[command][0]), 'usage': list[command][1]}))

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(config.get('internets', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'internets:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(config.get('internets', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'internets:channel' configuration option: %s" % err)
			raise

		self.bind_admin_commands()

	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.users.start()
		self.auth.start()
		self.antiflood.start()

	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('internets', 'nick'),
				self.config.get('internets', 'user'),
				self.config.get('internets', 'host'),
				self.config.get('internets', 'gecos'),
				self.config.get('internets', 'modes'),
				self.config.get('internets', 'nspass'),
				join_chans = [self.chan])
		except Exception, err:
			self.log.exception("Error creating user for internets module, is the config file correct? (%s)" % err)

		try:
			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.commands_private = cmd_private.PrivateCommandManager()
			self.commands_user = cmd_user.UserCommandManager()
		except Exception, err:
			self.log.exception('Error initializing core subsystems for internets module (%s)' % err)
			raise

		self.elog.debug('Started core subsystems.')

		try:
			self.channels = sys_channels.ChannelManager(self)
			self.users = sys_users.UserManager(self)
			self.auth = sys_auth.AuthManager(self)
			self.antiflood = sys_antiflood.AntiFloodManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for internets module (%s)' % err)
			raise

		self.elog.debug('Started subsystems.')

		try:
			try:
				self.bing = bing.Bing(self.config.get('internets', 'bing_appid'))
			except Exception, err:
				self.log.exception('Error initializing internets bing API (%s)' % err)
			self.nsp = calc.NumericStringParser()
			self.google = google.Google()
			self.imdb = imdb.Imdb()
			self.ipinfo = ipinfo.IpInfo(self.config.get('internets', 'key_ipinfodb'))
			self.lastfm = lastfm.LastFm(self.config.get('internets', 'key_lastfm'))
			self.quotes = quotes.Quotes(self.config.get('internets', 'key_fml'))
			self.urbandictionary = urbandictionary.UrbanDictionary()
			self.urls = urls.Urls(self.config.get('internets', 'user_bitly'), self.config.get('internets', 'key_bitly'))
			self.weather = weather.Weather(self.config.get('internets', 'key_openweathermap'))
			self.wolfram = wolfram.Wolfram(self.config.get('internets', 'key_wolframalpha'))
			self.wordnik = words.Words(self.config.get('internets', 'key_wordnik'))
		except Exception, err:
			self.log.exception('Error initializing internets module (%s)' % err)
			raise

		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.log.debug('Joined channels.')

		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for internets module (%s)' % err)
			raise

		self.initialized = True
		self.online = True
		self.elog.debug('Started threads.')
		return True

	def shutdown(self):
		PSModule.shutdown(self)

		if hasattr(self, 'antiflood'):
			self.antiflood.stop()

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'users'):
			if self.initialized:
				self.users.force()

			self.users.stop()
			self.users.db_close()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

		internets_modules = [module for module in sys.modules if module.startswith('modules.internets')]

		for module in internets_modules:
			sys.modules.pop(module)

		self.parent.quitFakeUser(self.uid)

	def getVersion(self):
		return self.VERSION

	def join(self, channel):
		self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
		self.parent.join_channel(self.uid, channel)

	def part(self, channel):
		self.parent.sendMessage("PART", channel, prefix = self.uid)
		self.parent.part_channel(self.uid, channel)

	def errormsg(self, target, message):
		self.msg(target, '@b@c4Error:@o %s' % message)

	def usagemsg(self, target, description, examples):
		message = '@errsep @bUsage@b %s @errsep' % description

		if examples != None:
			message += ' @bExamples@b %s @errsep' % ', '.join(examples)

		self.msg(target, message)

	def msg(self, target, message):
		if message != '':
			self.parent.privMsg(target, format_ascii_irc(message), self.uid)

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count

	def notice(self, target, message):
		if message != '':
			self.parent.notice(target, format_ascii_irc(message), self.uid)

	def execute(self, manager, command, argument, channel, sender, userinfo):
		full_command = '%s%s' % (command, ' %s' % argument if len(argument) else '')
		cmd = manager.get_command(command)

		if cmd == None:
			self.msg(channel, manager.invalid)
			self.elog.debug('Parsed command @b%s@b: invalid command.' % full_command)
			return

		if self.users.is_banned(sender) or self.antiflood.check_user(sender, command, argument):
			user = self.users[sender]
			message = 'You were banned by @b%s@b.' % user.ban_source

			if user.ban_reason != None:
				message += ' Reason: @b%s@b.' % user.ban_reason

			if user.ban_expiry != None:
				message += ' Expires: @b%s@b.' % datetime.fromtimestamp(user.ban_expiry)

			self.notice(sender, message)
			self.elog.debug('Parsed command @b%s@b: user is banned.' % full_command)
			return

		self.elog.command('%s%s > %s' % (sender, ':%s' % channel if channel != sender else '', full_command))

		parser = erepparser.ErepublikParser(add_help_option = False, option_class = erepparser.ErepublikParserOption)
		cmd_type = cmd[1]
		cmd_args = cmd[3]

		parser.add_option('-?', '--help', action = 'store_true')

		for cmd_arg in cmd_args:
			parser.add_option(cmd_arg[1], '--' + cmd_arg[0], **cmd_arg[3])

		try:
			(popts, pargs) = parser.parse_args(args = argument.split(' '))
		except erepparser.ErepublikParserError, err:
			self.msg(channel, str(err)) #TODO: Avoid str, use unicode.
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: invalid options.' % full_command)
			return

		if popts.help == True:
			manager.commands['help'][0](self, manager, {}, command, channel, sender)
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: help intercepted.' % full_command)
			return

		opt_dict = {}
		larg = ' '.join(pargs).strip()
		is_offline = True

		for cmd_arg in cmd_args:
			parg = getattr(popts, cmd_arg[0])

			if parg != None:
				if len(cmd_arg) <= 4 or not (cmd_arg[4] & cmd_manager.ARG_OFFLINE):
					is_offline = False

				if len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_YES) and larg == '':
					self.msg(channel, 'Error: %s option requires an argument.' % cmd_arg[1])
					parser.destroy()
					self.elog.debug('Parsed command @b%s@b: option constraint was broken.' % full_command)
					return

				opt_dict[cmd_arg[0]] = parg
			elif len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_OFFLINE and cmd_arg[4] & cmd_manager.ARG_OFFLINE_REQ):
				is_offline = False

		if not self.online and ((len(pargs) > 0 and not (cmd_type & cmd_manager.ARG_OFFLINE)) or not is_offline):
			self.notice(sender, 'The eRepublik API is offline. Please retry later.')
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: offline.' % full_command)
			return

		if (cmd_type & cmd_manager.ARG_YES) and (larg == None or larg == ''):
			self.notice(sender, '@bUsage@b: %s @b%s@b' % (command, cmd[4] if len(cmd) > 4 else 'argument'))
		else:
			try:
				cmd[0](self, manager, opt_dict, larg, channel, sender, userinfo)
			except Exception, e:
				tb = traceback.extract_tb(sys.exc_info()[2])
				longest = 0

				for entry in tb:
					length = len(entry[2])

					if length > longest:
						longest = length

				self.elog.exception('%s%s > @b%s@b: %s' % (sender, ':%s' % channel if channel != sender else '', full_command, e))
				self.log.exception("internets error!")

				for entry in tb:
					self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))

				self.msg(channel, 'An exception occurred and has been reported to the developers. If this error persists please do not use the faulty command until it has been fixed.')

		parser.destroy()
		self.elog.debug('Parsed command @b%s@b: execution terminated.' % full_command)

	def internets_TMODE(self, prefix, params):
		if not self.initialized:
			return

		channel = params[1]
		modes = params[2]

		if modes == '-z' and channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)

	def internets_NOTICE(self, prefix, params):
		if not self.initialized:
			return

		uid, user = self.parent.get_user(params[0])
		foo, userinfo = self.parent.get_user(prefix)

		if uid != self.uid or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = params[1].strip()
		except:
			return

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
				nick = strip_ascii_irc(sp[1])
				channel = strip_ascii_irc(sp[7])
				self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (self.nick, channel))

			return

		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][0:len(sp[5]) - 1]

		if 'Founder' in sp[2]:
			self.auth.accept(nick)
		else:
			self.auth.reject_not_founder(nick, channel)

	def internets_PRIVMSG(self, prefix, params):
		if not self.initialized:
			return

		foo, userinfo = self.parent.get_user(prefix)
		bar, myself = self.parent.get_user(self.uid)

		sender = userinfo['nick']
		channel = params[0]
		msg = params[1].strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		if channel == myself['nick'] and command.startswith(self.commands_private.prefix):
			self.elog.debug('Deferred parsing of private message (sender: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, command, argument))
			threading.deferToThread(self.execute, None, None, self.commands_private, command, argument, sender, sender, userinfo)
		elif self.channels.is_valid(channel) and command.startswith(self.commands_user.prefix):
			self.elog.debug('Deferred parsing of channel message (sender: @b%s@b, channel: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, channel, command, argument))
			threading.deferToThread(self.execute, None, None, self.commands_user, command, argument, channel, sender, userinfo)

	def getCommands(self):
		return self.commands_admin

	def getHooks(self):
		return (('tmode', self.internets_TMODE),
			('notice', self.internets_NOTICE),
			('privmsg', self.internets_PRIVMSG)
			)
	
	def get_location(self, opts, arg, channel, sender):
		nick = None
		location = None
		
		if 'nick' in opts:
			nick = arg
		elif arg == '':
			nick = sender
		else:
			location = arg

		if nick:
			location = self.users.get(nick, 'location')

		if not location:
			if 'nick' in opts:
				self.msg(channel, 'No location found linked to nick %s.' % arg)
			else:
				self.msg(channel, 'No location found linked to your nick. To link one type: @b.register_location <location>@b')

		return location
