# psm_deaf.py
# Written by Adam (Adam@rizon.net)

from psmodule import *
import fnmatch
import time

class PSModule_deaf(PSModule):
    class deaf:
        mask = None
        creator = None
        time = 0
        
        def __init__(self, m, c, t):
            self.mask = m
            self.creator = c
            self.time = t

    deafs = []
    
    def findDeaf(self, mask):
        for d in self.deafs:
            if fnmatch.fnmatch(mask.lower(), d.mask.lower()):
                return d
        return None

    def startup(self):
        if not PSModule.startup(self):
            return False
        self.log = logging.getLogger(__name__)

        try:
            self.dbp.execute("CREATE TABLE IF NOT EXISTS deaf (mask VARCHAR(255), creator VARCHAR(255), created INT(11), PRIMARY KEY (mask));")
        except Exception, err:
            self.log.exception("Error creating table for deaf module (%s)" % err)
            raise
        
        try:
            self.dbp.execute("SELECT * FROM deaf")
            for row in self.dbp.fetchall():
                self.deafs.append(self.deaf(row[0], row[1], row[2]))
        except Exception, err:
            self.log.exception("Error loading deaf entries from database: %s" % err)
            raise
        
        return True
    
    def cmd_deafAdd(self, source, target, pieces):
        if len(pieces) == 0:
            return False
        
        uid, user = self.parent.get_user(source)
        
        d = self.findDeaf(pieces[0])
        if d:
            self.parent.privMsg(target, pieces[0] + " is already covered by " + d.mask)
            return True
        
        try:
            self.dbp.execute("INSERT INTO deaf (mask, creator, created) VALUES(%s, %s, %s)", (pieces[0], user['nick'], time.time()))
            self.deafs.append(self.deaf(pieces[0], user['nick'], time.time()))
            self.parent.privMsg(target, pieces[0] + " added to the deaf list")
        except Exception, err:
            self.log.exception("Error inserting entry into deaf database: %s" % (err))
        
        return True
    
    def cmd_deafDel(self, source, target, pieces):
        if len(pieces) == 0:
            return False
        
        d = self.findDeaf(pieces[0])
        if not d:
            self.parent.privMsg(target, pieces[0] + " is not on the deaf list")
            return True

        while True:
            d = self.findDeaf(pieces[0])
            if not d:
                break
            try:
                self.dbp.execute("DELETE FROM deaf WHERE `mask` = %s", d.mask)
                self.deafs.remove(d)
                self.parent.privMsg(target, d.mask + " removed from the deaf list")
            except Exceltion, err:
                self.log.exception("Error removing entry from deaf database: %s" % (err))

        return True
    
    def cmd_deafList(self, source, target, pieces):
        if len(self.deafs) == 0:
            self.parent.privMsg(target, "Deaf list is empty.")
        else:
            for d in self.deafs:
                dtime = time.strftime("%b %d %Y at %H:%M %Z", time.gmtime(d.time))
                self.parent.privMsg(target, d.mask + " set by " + d.creator + " on " + dtime)
        return True
    
    def hook_UID(self, prefix, params):
        d = self.findDeaf(params[0] + "!" + params[4] + "@" + params[5])
        if d:
            self.parent.sendMessage("ENCAP", "*", "SVSMODE", params[0], params[2], "+D", prefix=self.parent.sid)
            self.parent.changeUmodes(params[7], "+D")
            self.parent.privMsg(self.logchan, "Deaf: Setting +D on " + params[0] + "!" + params[4] + "@" + params[5] + " for matching " + d.mask)
        return True
    
    def hook_NICK(self, prefix, params):
        uid, user = self.parent.get_user(params[0])
        if user:
            d = self.findDeaf(user['nick'] + "!" + user['user'] + "@" + user['host'])
            if d:
                self.parent.sendMessage("ENCAP", "*", "SVSMODE", user['nick'], user['ts'], "+D", prefix=self.parent.sid)
                self.parent.changeUmodes(uid, "+D")
                self.parent.privMsg(self.logchan, "Deaf: Setting +D on " + user['nick'] + "!" + user['user'] + "@" + user['host'] + " for matching " + d.mask)
        return True
    
    def getCommands(self):
        return (
            ('add', {
            'permission' : 'D',
            'callback' : self.cmd_deafAdd,
            'usage' : "<user> - Add a usermask to the deaf list"}),
            ('del', {
            'permission' : 'D',
            'callback' : self.cmd_deafDel,
            'usage' : "<user> - Remove a usermask from the deaf list"}),
            ('list', {
            'permission' : 'D',
            'callback' : self.cmd_deafList,
            'usage' : "- Shows all deaf entries"}),
        )
        
    def getHooks(self):
        return (
           ('uid', self.hook_UID),
           ('nick', self.hook_NICK),
        )
    