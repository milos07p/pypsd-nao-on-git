#!/usr/bin/python pseudoserver.py
#psm-limitserv.py
# module for pypseudoserver
# written by celestin - martin <martin@rizon.net>

import sys
import types
from untwisted.istring import istring
from psmodule import *
from limitserv import cmd_admin
from limitserv import sys_auth, sys_channels, sys_limits, sys_log, sys_options
from limitserv.utils import *

class PSModule_limitserv(PSModule):
	VERSION = 0.1
	uid = ""
	initialized = False
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		self.limit_monitor.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, PSModule_limitserv)
		setattr(PSModule_limitserv, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(config.get('limitserv', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'limitserv:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(config.get('limitserv', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'limitserv:channel' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def startup(self):
		if not PSModule.startup(self):
			return False

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS limitserv_chans (item SMALLINT(5) NOT NULL AUTO_INCREMENT, channel VARCHAR(35), \
				PRIMARY KEY (item), UNIQUE KEY (channel));")
		except Exception, err:
			self.log.exception("Error creating table for limitserv module (%s)" % err)
			raise
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('limitserv', 'nick'),
				self.config.get('limitserv', 'user'),
				self.config.get('limitserv', 'host'),
				self.config.get('limitserv', 'gecos'),
				self.config.get('limitserv', 'modes'),
				self.config.get('limitserv', 'nspass'),
				join_chans = [self.chan],
				version="PyPsd LimitServ v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating limitserv module user: %s" % err)
			raise

		try:
			self.options = sys_options.OptionManager(self)
			self.auth = sys_auth.AuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
			self.elog = sys_log.LogManager(self)
			self.limit_monitor = sys_limits.LimitManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for limitserv module (%s)' % err)
			raise
			
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for limitserv module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True
	
	def shutdown(self):
		PSModule.shutdown(self)

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()
		
		if hasattr(self, 'limit_monitor'):
			self.limit_monitor.stop()
			
		limitserv_modules = [module for module in sys.modules if module.startswith('modules.limitserv')]

		for module in limitserv_modules:
			sys.modules.pop(module)

		self.parent.quitFakeUser(self.uid)

	def join(self, channel):
#		self.parent.sendMessage("SJOIN", "%d" % self.parent.mkts(), channel, "+ :@%s" % self.uid, prefix=self.parent.sid)
		self.parent.sendMessage("JOIN", str(self.parent.mkts()), channel, '+', prefix = self.uid)
		self.parent.join_channel(self.uid, channel)
#		self.parent.sendMessage("MODE", channel, "+o %s" % self.nick, prefix=self.uid)
#		self.parent.changeCmodes(channel, "+o", "%s" % self.nick) # ChanServ deops LimitServ if pypsd isn't ulined in services
		self.msg('ChanServ', 'OP %s' % channel)                   # and the channel is not empty. For now, use /cs op

	def part(self, channel):
		uid, user = self.parent.get_user(self.uid)

		if istring(channel) in user['channels']:
			self.parent.sendMessage("PART", channel, prefix=self.uid)
			self.parent.part_channel(self.uid, channel)

	def msg(self, target, message):
		if message != '':
			self.parent.privMsg(target, format_ascii_irc(message), self.uid)

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			self.parent.notice(target, format_ascii_irc(message), self.uid)
			
## Begin event hooks
	def limitserv_NOTICE(self, prefix, params):
		if not self.initialized:
			return
		
		uid, user = self.parent.get_user(params[0])
		foo, userinfo = self.parent.get_user(prefix)

		if uid != self.uid or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = params[1].strip()
		except:
			return

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
					nick = strip_ascii_irc(sp[1])
					channel = strip_ascii_irc(sp[7])
					self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (user['nick'], channel))
			return

		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][0:len(sp[5]) - 1]
		
		if 'Founder' in sp[2]:
			if self.auth.requests[nick].action == 'request':
				cname, chan = self.parent.get_channel(channel)
				if len(chan['members']) < self.options.get('required_users', int, 20):
					self.auth.reject_not_enough_users(nick, channel)
				else:
					self.auth.accept(nick)
			else:
				self.auth.accept(nick)
		else:
			self.auth.reject_not_founder(nick, channel)	

	def limitserv_PRIVMSG(self, prefix, params):
	# Parse ADD/DEL requests
		if not self.initialized:
			return

		bar, myself = self.parent.get_user(self.uid)
		channel = params[0]
		if channel != myself['nick']:
			return # not a private message, ignore
		
		foo, userinfo = self.parent.get_user(prefix)
		sender = userinfo['nick']

		msg = params[1].strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			arg = ''
		else:
			command = msg[:index]
			arg = msg[index + 1:]

		command = command.lower()
		
		if command == 'request':
			if not arg:
				self.msg(sender, '@bUsage@b: request @b#channel@b')
				return
			
			if self.channels.is_valid(arg):
				self.msg(sender, "I'm already in @b%s@b." % arg)
			elif self.channels.is_banned(arg):
				chan = self.channels[arg]
				self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender))
				message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)
		
				if chan.ban_reason != None:
					message += ' Reason: @b%s@b.' % chan.ban_reason
		
				if chan.ban_expiry != None:
					message += ' Expires: @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)
		
				self.msg(sender, message)
			else:
				self.auth.request(sender, arg, 'request')
		elif command == 'remove':
			if not arg:
				self.msg(sender, '@bUsage@b: request @b#channel@b')
				return
			
			if not self.channels.is_valid(arg):
				self.msg(sender, "I'm not in @b%s@b." % arg)
			else:
				self.auth.request(sender, arg, 'remove')
		elif command == 'help' or command == 'hi' or command == 'hello':
			self.msg(sender, '@brequest@b requests a channel (must be founder)')
			self.msg(sender, '@bremove@b removes a channel (must be founder)')
			self.msg(sender, '@bhelp@b displays help text')
			self.msg(sender, 'For more information, see: http://forum.rizon.net/showthread.php?981-Rizon-LimitServ')
		else:
			self.msg(sender, 'Invalid message. Say help for a list of valid messages.')
	
	def limitserv_TMODE(self, prefix, params):
		if not self.initialized:
			return

		channel = params[1]
		modes = params[2]

		if modes == '-z' and channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def limitserv_ADDCHANNEL(self, prefix, params):
		if not self.initialized:
			return
		
		if isinstance(params, basestring) or isinstance(params, istring):
			if self.channels.is_valid(params) and params not in self.limit_monitor:
				self.limit_monitor.insert(params)
		elif isinstance(params, list):
			if self.channels.is_valid(params[0]) and params[0] not in self.limit_monitor:
				self.limit_monitor.insert(params[0])
	
	def limitserv_QUIT(self, prefix, params):
		if not self.initialized or prefix == self.uid or len(params) < 2:
			return
		
		for chan in params[1:]:
			if self.channels.is_valid(chan) and chan not in self.limit_monitor:
				self.limit_monitor.insert(chan)
	
	def getCommands(self):
		return self.commands_admin
	
	def getHooks(self):
		return (('privmsg', self.limitserv_PRIVMSG),
					('notice', self.limitserv_NOTICE),
					('tmode', self.limitserv_TMODE),
					('join', self.limitserv_ADDCHANNEL),
					('part', self.limitserv_ADDCHANNEL),
					('quit', self.limitserv_QUIT),
					('kick', self.limitserv_ADDCHANNEL)
                    )
