# module for pypseudoserver
# written by radicand
# connects pypsd with c-proxycheck

import untwisted
from untwisted.process import Process
from untwisted.istring import istring
import re
from psmodule import *

# 176.31.89.107 ho:3128 open
proxybridge_regex_test = re.compile("^([0-9.]+) ([^:]+):(\d+) open")

class ProxyBridgeProcess(Process):
	log = logging.getLogger(__name__)
	banned = False
	callback = None
	ip = None
	user = None
	type = None
	kwargs = None

	def __init__(self, reactor, bin, args, callback, ip, user, type, kwargs):
		Process.__init__(self, reactor, bin, args)

		self.callback = callback
		self.ip = ip
		self.user = user
		self.type = type
		self.kwargs = kwargs
	
	def OnProcessRead(self, data):
		data = data.strip()
		self.log.debug("SCAN_DATA: " + data)

		if self.banned:
			return

		try:
			test = proxybridge_regex_test.match(data)
			if test:
				self.banned = True
				ip = test.group(1)
				port = int(test.group(3))
				service = test.group(2)
				self.log.debug("SCAN_RESULT: Port %d was tainted on %s" % (port, ip))
				self.callback([port, service], ip, self.user, self.type, **self.kwargs)
			else:
				self.log.debug("SCAN_RESULT: Regex fail on %s" % data)
		except Exception, ex:
			self.log.debug("Exception parsing proxycheck: %s (%s)" % (ex, data))
	
	def OnProcessEnd(self):
		if not self.banned:
			self.log.debug("SCAN_RESULT: No result on " + self.ip)
			self.callback(None, self.ip, self.user, self.type, **self.kwargs)
		self.log.debug("SCAN_ENDED: Scan process ended")

class PSModule_proxybridge(PSModule):
	BOPM_LIST = []
	BOPM_TOTAL = 0
	BOPM_BANNED = 0
	VERSION = 6
		
	uid = ""
	sourceip = ""
	destip = ""
	destport = ""
		
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)
						
		try:
			self.sourceip = istring(self.config.get('bopm', 'sourceip'))
			self.sourcehost = istring(self.config.get('bopm', 'sourcehost'))
			self.destip = istring(self.config.get('bopm', 'destip'))
			self.destport = istring(self.config.getint('bopm', 'destport'))
			self.sockettimeout = istring(self.config.getint('bopm', 'sockettimeout'))
			self.pc_bin = istring(self.config.get('bopm', 'pc_bin'))
			self.chkstring = istring(self.config.get('bopm', 'chkstring'))
		except Exception, err:
			self.log.exception("Error setting bopm module options, are they defined in config? (%s)" % err)
			raise
				
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('bopm', 'nick'),
				self.config.get('bopm', 'user'),
				self.config.get('bopm', 'host'),
				self.config.get('bopm', 'gecos'),
				self.config.get('bopm', 'modes'),
				self.config.get('bopm', 'nspass'),
				version="PyPsd Blacklist Open Proxy Monitor v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating user for BOPM module, do the options exist in config? (%s)" % err)
			raise
					
		try:
			num = self.dbp.execute("SELECT num FROM record WHERE name='bopm-total';")
			if num:
				self.BOPM_TOTAL = self.dbp.fetchone()[0]

			num = self.dbp.execute("SELECT num FROM record WHERE name='bopm-found';")
			if num:
				self.BOPM_BANNED = self.dbp.fetchone()[0]

		except Exception, err:
			self.log.exception("Unable to fetch historical records for bopm: %s" % err)
	
		return True
		
	def shutdown(self):
		PSModule.shutdown(self)
		try:
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('bopm-total', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.BOPM_TOTAL,self.BOPM_TOTAL))
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('bopm-found', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.BOPM_BANNED,self.BOPM_BANNED))
		except Exception, err:
			self.log.error("Error updating historical records for bopm module: %s" % err)

		self.parent.quitFakeUser(self.uid)

	def isIP(self, ip):
		"""Checks the supplied argument to see if it validates as an IP address"""
				
		if re.match("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", ip):
			return True
		else: return False
				
	def bopm_CHECK(self, ip, user, type, **kwargs):
		"""Check host for vulnerable proxy stuff
		Param is the ip to check
		"""

		if ip == "127.0.0.1" or ip == "255.255.255.255" or ip == "0":
			return

		self.log.debug("Scanning %s..." % ip)

		cmd = "%s -b %s -c chat::%s -d %s:%d -s -aaaa -t %d %s" % (self.pc_bin, self.sourceip, self.chkstring, self.destip, self.destport, self.sockettimeout, ip)
		proto = kwargs.get('proto')
		if proto:
			cmd += " -p %s" % (proto,)
			#self.log.debug(cmd)

		try:
			ProxyBridgeProcess(self.parent.socket._reactor, self.pc_bin, cmd.split(" "), self.bopm_HANDLE, ip, user, type, kwargs)
		except Exception, err:
			self.log.exception("Error spawning proxybridge: %s (%s)" % (err, cmd))

	def bopm_HANDLE(self, cb_list, ip, user, type, **kwargs):
		"""Callback for the main bopm checker function"""

		source = kwargs.get('source', self.logchan)
				
		if not cb_list:
			if type != "auto":
				self.parent.privMsg(source,
						"%s passed the manual BOPM scan." % ip, self.uid)
			return
		elif type == "host":
			self.parent.privMsg(source, "BOPM: Manually Banned *@%s Match: %d/%s" %
					(ip, cb_list[0], cb_list[1]), self.uid)
			if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
			else: serv = "GeoServ"
			self.parent.privMsg(serv, "akill add +3d *@%s Insecure proxy found, %s:%d - \
					Visit http://en.wikipedia.org/wiki/Open_proxy for more information." %
					(ip, cb_list[1], cb_list[0]), self.uid)

			if "dnsbl_admin" in self.parent.modules:
				uid2, user2 = self.parent.get_user(self.uid)
				self.parent.modules['dnsbl_admin'].add(ip, "1", "Open Proxy found by %s (%s has %s:%d insecured)" %
						(user2['nick'],ip,cb_list[1],cb_list[0]))
			return
		elif not user:
			return

		if type == "auto":
			self.BOPM_BANNED += 1

		self.parent.privMsg(source, "BOPM: Banned %s!%s@%s Match: %d/%s (#%d/%d) Chans: %s" %
				(user['nick'], user['user'], user['ip'],
				cb_list[0], cb_list[1], self.BOPM_BANNED, self.BOPM_TOTAL, ' '.join(user['channels'])), self.uid)

		if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
		else: serv = "GeoServ"
		self.parent.privMsg(serv, "akill add +3d *@%s Insecure proxy found, %s:%d - \
				Visit http://en.wikipedia.org/wiki/Open_proxy for more information." %
				(user['ip'], cb_list[1], cb_list[0]), self.uid)

		if "dnsbl_admin" in self.parent.modules:
				uid2, user2 = self.parent.get_user(self.uid)
				self.parent.modules['dnsbl_admin'].add(user['ip'], "1", "Open Proxy found by %s (%s!%s@%s has %s:%d insecured)" %
						(user2['nick'],user['nick'],user['user'],user['ip'],cb_list[1],cb_list[0]))

## Begin event hooks
	def bopm_UID(self, prefix, params):
		"""Called when a user signs on
		Informs them of the US statute and triggers the thread to scan
		"""

		#if self.parent.flood: return

		if not self.isIP(params[6]): return # ipv6

		self.BOPM_TOTAL += 1

		#self.parent.privMsg(self.logchan, "BOPM: Checking %s!%s@%s (#%d)" %
		#		(params[0], params[4], params[6], self.BOPM_TOTAL),  self.uid)
		self.parent.notice(params[7],
				"Pursuant to United States Code (USC), Title 18, Part 1, Section 2701 Part (c),",
				self.uid)
		self.parent.notice(params[7],
				"%s will do a passive scan of your IP address to check for open proxies." %
				self.config.get('server', 'network'), self.uid)
		self.parent.notice(params[7],
				"If you do not consent to this scan, disconnect now. These scans are harmless and",
				self.uid)
		self.parent.notice(params[7],
				"will originate from %s (%s). For more information see #help." %
				(self.sourcehost, self.sourceip), self.uid)

		self.bopm_CHECK(params[6], self.parent.user_t[params[7]], "auto")
## End event hooks
				
## Begin command hooks		
	def cmd_nickscan(self, source, target, pieces):
		"""Scans the host associated with the supplied nickname"""
				
		if not pieces: return False
						
		uid, user = self.parent.get_user(pieces[0])
		if not user:
			self.parent.privMsg(target,
					"%s is not an online user, cannot scan" %
					(pieces[0]), self.uid)
			return True
		
		ip = user['ip']
		if not self.isIP(ip):
			self.parent.privMsg(target,
					"%s (%s) is spoofed or invalid, cannot scan" % (pieces[0], ip), self.uid)
			return True
		
		self.BOPM_TOTAL += 1
		self.parent.privMsg(target,
				"Initiating manual nick BOPM scan against %s/%s..." %
				(pieces[0], ip), self.uid)

		self.bopm_CHECK(ip, user, "nick", source=target)
		return True

	def cmd_bhscan(self, source, target, pieces):
		"""Manually scan a host and return the result, banning if necessary"""
		
		if not pieces: return False

		ip = pieces[0]
		if not self.isIP(ip):
			self.parent.privMsg(target,
	   				"%s is an invalid ip, cannot scan" %
					(ip,), self.uid)
			return True

		if len(pieces) > 1:
			proto = pieces[1]
		else: proto = None

		self.parent.privMsg(target,
				"Initiating manual host BOPM scan against %s..." %
				(ip,), self.uid)

		self.bopm_CHECK(ip, None, "host", source=target, proto=proto)
		return True
				
	def cmd_reload(self, source, target, pieces):
		"""Reloads bomp list
		TODO: need to move this out of the config.ini
		"""
				
		if self.reload():
			self.parent.privMsg(target,
					"BOPM reload successful", self.uid)
		else:
			self.parent.privMsg(target,
					"BOPM reload failed, check your config file", self.uid)
		return True
## End Command hooks

	def getCommands(self):
		return (('reload', {
					'permission':'N',
					'callback':self.cmd_reload,
					'usage':"-reloads the port and service list from the config file"}),
				('hscan', {
					'permission':'b',
					'callback':self.cmd_bhscan,
					'usage':"<host/ip> [port] - manually scan a host (and optionally on one specific port)"}),
				('nscan', {
					'permission':'b',
					'callback':self.cmd_nickscan,
					'usage':"<nick> - issue a BOPM scan for nick"}),
				)

	def getHooks(self):
		return (('uid', self.bopm_UID),)
