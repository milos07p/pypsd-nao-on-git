#!/usr/bin/python pseudoserver.py
#psm-debugger.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
import sys

class PSModule_debugger(PSModule):
    VERSION = 1

    def cmd_versions(self, source, target, pieces):
        self.parent.privMsg(target, "Python v%s" % (sys.version_info,))
        return True

    def cmd_certfp(self, source, target, pieces):
        y = len(filter(lambda x: x['certfp'], self.parent.user_t.values()))
        self.parent.privMsg(target, "%d users are connected with a certificate." % (y,))
        return True

    def cmd_dumpgraph(self, source, target, pieces):
        """Writes graph data to a file"""
        f = open("debug.log", "w")
        f.write("---SERVERS---\n")
        for k,v in self.parent.server_t.iteritems():
            t = "%s,%s,%s\n" % (k, v['uplink_sid'], v['host'])
            f.write(t.encode('utf-8'))
        self.parent.privMsg(target, "Data dumped.")
        f.close()
        return True

    def cmd_searchlog(self, source, target, pieces):
        """Searches through the py.log for terms

        Searches backward and then displays the output in reverse
        """

        if not pieces: return False
        
        curcount = 0
        try:
            limiter = int(pieces[-1])
            chop = -1
        except (ValueError, IndexError):
            limiter = 5
            chop = None

        term = " ".join(pieces[:chop])

        results = []
        for line in open("py.log", "r"):
            line = self.parent.c2u(line)
            if term in line:
                results.insert(0, line)

        for out in results:
            self.parent.notice(source, out)
            if curcount > limiter: break
            curcount = curcount + 1

        return True
    
    def cmd_dumpdata(self, source, target, pieces):
        """Writes key variables to a file"""
        f = open("debug.log", "w")
        f.write("---USER---\n")
        for k,v in self.parent.user_t.iteritems():
            t = "%s: %s\n" % (k,v)
            f.write(t.encode('utf-8'))
        f.write("---SERVER---\n")
        for k,v in self.parent.server_t.iteritems():
            t = "%s: %s\n" % (k,v)
            f.write(t.encode('utf-8'))
        f.write("---CHANNEL---\n")
        for k,v in self.parent.channel_t.iteritems():
            t = "%s: %s\n" % (k,v)
            f.write(t.encode('utf-8'))
        self.parent.privMsg(target, "Data dumped.")
        f.close()
        return True

    def cmd_openmanhole(self, source, target, pieces):
        """ Opens a ssh manhole
        exposes 'self' as the parent protocol"""

        try:
            from util import manhole
            authkeyfile = self.config.get('debugger', 'authkeyfile')
            port = self.config.getint('debugger', 'port')
        except Exception, err:
            self.log.exception("Error setting up manhole: %s" % (err,))
            self.parent.privMsg(target, "Manhole not opened, setup error")
            return True

        try:
            manhole.openSSHManhole(authkeyfile, {'self':self.parent}, portNum=port)
        except Exception, err:
            self.log.exception("Error opening manhole: %s" % (err,))
            self.parent.privMsg(target, "Manhole not opened, system error")
        else:
            self.parent.privMsg(target, "Manhole opened")

        return True

    def getCommands(self):
        return (
            ('certcount', {
	    'permission':'d',
            'callback':self.cmd_certfp,
            'usage':"- prints the number of users connected with a certificate"}),
            ('dumpgraph', {
	    'permission':'d',
            'callback':self.cmd_dumpgraph,
            'usage':"- writes server graph data to the debug log"}),
            ('versions', {
	    'permission':'d',
            'callback':self.cmd_versions,
            'usage':"- prints version information"}),
            ('dumpdata', {
	    'permission':'d',
            'callback':self.cmd_dumpdata,
            'usage':"- writes key server internal data to a debug log"}),
            ('searchlog', {
	    'permission':'d',
            'callback':self.cmd_searchlog,
            'usage':"<term> [number of lines,def=5] - looks through the py.log and returns the most current matches"}),
            ('openmanhole', {
	    'permission':'d',
            'callback':self.cmd_openmanhole,
            'usage':"- opens a ssh python shell to the server"}),
        )
